<?php

namespace src\controllers;

use src\libraries\Rumus;

class Index
{
	function __construct()
	{
		$data = ['bangun_datar' => null];

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'index.php';
		include VIEW_PATH . 'footer.php';
	}
}