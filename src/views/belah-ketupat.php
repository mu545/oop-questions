<form action="?page=belah-ketupat" method="POST">
    <h1>Belah Ketupat</h1>
    <input type="number" name="diagonal1" <?php echo isset($_POST['diagonal1']) ? 'value="' . $_POST['diagonal1'] . '"' : ''; ?> placeholder="Diagonal 1">
    <input type="number" name="diagonal2" <?php echo isset($_POST['diagonal2']) ? 'value="' . $_POST['diagonal2'] . '"' : ''; ?> placeholder="Diagonal 2">
    <input type="number" name="sisi" <?php echo isset($_POST['sisi']) ? 'value="' . $_POST['sisi'] . '"' : ''; ?> placeholder="Sisi">
    <div>
        <button class="btn-luas" type="submit" name="luas">Luas</button>
        <button class="btn-keliling" type="submit" name="keliling">Keliling</button>
    </div>
</form>