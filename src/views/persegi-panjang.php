<form action="?page=persegi-panjang" method="POST">
    <h1>Persegi Panjang</h1>
    <input type="number" name="panjang" <?php echo isset($_POST['panjang']) ? 'value="' . $_POST['panjang'] . '"' : ''; ?> placeholder="Panjang">
    <input type="number" name="lebar" <?php echo isset($_POST['lebar']) ? 'value="' . $_POST['lebar'] . '"' : ''; ?> placeholder="Lebar">
    <div>
        <button class="btn-luas" type="submit" name="luas">Luas</button>
        <button class="btn-keliling" type="submit" name="keliling">Keliling</button>
    </div>
</form>