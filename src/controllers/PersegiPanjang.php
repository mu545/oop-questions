<?php

namespace src\controllers;

use src\libraries\Rumus;

class PersegiPanjang
{
	function __construct()
	{
		$data = ['panjang' => 0, 'lebar' => 0, 'bangun_datar' => null];

		/* untuk mengetahui luas atau keliling dari persegi panjang
		 * terlebih dahulu ketahui panjang, lebar dari persegi
		 */
		if (isset($_POST['panjang'])) {
			$data['panjang'] = (int) $_POST['panjang'];
		}

		if (isset($_POST['lebar'])) {
			$data['lebar'] = (int) $_POST['lebar'];
		}

		$data['bangun_datar'] = new Rumus\PersegiPanjang();
		$data['bangun_datar']->setPanjang($data['panjang']);
		$data['bangun_datar']->setLebar($data['lebar']);

		if (isset($_POST['luas'])) {
			$data['bangun_datar']->hitungLuas();
		} else if (isset($_POST['keliling'])) {
			$data['bangun_datar']->hitungKeliling();
		}

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'persegi-panjang.php';
		include VIEW_PATH . 'footer.php';
	}
}