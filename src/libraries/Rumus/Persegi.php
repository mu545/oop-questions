<?php

namespace src\libraries\Rumus;

class Persegi extends Rumus
{
	private $sisi		= 0;
	private $luas		= 0;
	private $keliling	= 0;

	public function setSisi(Int $sisi)
	{
		$this->sisi = $sisi;
	}

	public function hitungLuas()
	{
		$this->luas = $this->sisi * $this->sisi;
	}

	public function hitungKeliling()
	{
		$this->keliling = 4 * $this->sisi;
	}

	public function getLuas()
	{
		return $this->luas;
	}

	public function getKeliling()
	{
		return $this->keliling;
	}
}