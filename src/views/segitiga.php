<form action="?page=segitiga" method="POST">
    <h1>Segitiga</h1>
    <input type="number" name="alas" <?php echo isset($_POST['alas']) ? 'value="' . $_POST['alas'] . '"' : ''; ?> placeholder="Alas">
    <input type="number" name="tinggi" <?php echo isset($_POST['tinggi']) ? 'value="' . $_POST['tinggi'] . '"' : ''; ?> placeholder="Tinggi">
    <input type="number" name="sisi_a" <?php echo isset($_POST['sisi_a']) ? 'value="' . $_POST['sisi_a'] . '"' : ''; ?> placeholder="Sisi a">
    <input type="number" name="sisi_b" <?php echo isset($_POST['sisi_b']) ? 'value="' . $_POST['sisi_b'] . '"' : ''; ?> placeholder="Sisi b">
    <input type="number" name="sisi_c" <?php echo isset($_POST['sisi_c']) ? 'value="' . $_POST['sisi_c'] . '"' : ''; ?> placeholder="Sisi c">
    <div>
        <button class="btn-luas" type="submit" name="luas">Luas</button>
        <button class="btn-keliling" type="submit" name="keliling">Keliling</button>
    </div>
</form>