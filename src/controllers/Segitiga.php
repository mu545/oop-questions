<?php

namespace src\controllers;

use src\libraries\Rumus;

class Segitiga
{
	function __construct()
	{
		$data = ['alas' => 0, 'tinggi' => 0, 'sisi_a' => 0, 'sisi_b' => 0, 'sisi_c' => 0, 'bangun_datar' => null];

		/* untuk mengetahui luas atau keliling dari segitiga
		 * terlebih dahulu ketahui alas, tinggi, sisi (a, b, c) dari segitiga
		 */
		if (isset($_POST['alas'])) {
			$data['alas'] = (int) $_POST['alas'];
		}

		if (isset($_POST['tinggi'])) {
			$data['tinggi'] = (int) $_POST['tinggi'];
		}

		if (isset($_POST['sisi_a'])) {
			$data['sisi_a'] = (int) $_POST['sisi_a'];
		}

		if (isset($_POST['sisi_b'])) {
			$data['sisi_b'] = (int) $_POST['sisi_b'];
		}

		if (isset($_POST['sisi_c'])) {
			$data['sisi_c'] = (int) $_POST['sisi_c'];
		}

		$data['bangun_datar'] = new Rumus\Segitiga();
		$data['bangun_datar']->setAlas($data['alas']);
		$data['bangun_datar']->setTinggi($data['tinggi']);
		$data['bangun_datar']->setSisiA($data['sisi_a']);
		$data['bangun_datar']->setSisiB($data['sisi_b']);
		$data['bangun_datar']->setSisiC($data['sisi_c']);

		if (isset($_POST['luas'])) {
			$data['bangun_datar']->hitungLuas();
		} else if (isset($_POST['keliling'])) {
			$data['bangun_datar']->hitungKeliling();
		}

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'segitiga.php';
		include VIEW_PATH . 'footer.php';
	}
}