<?php

require "vendor/autoload.php";

/* Controllers
 */
use src\controllers\Index;
use src\controllers\Persegi;
use src\controllers\PersegiPanjang;
use src\controllers\Lingkaran;
use src\controllers\BelahKetupat;
use src\controllers\Segitiga;

/* Environments variable
 */
define('VIEW_PATH', __DIR__ . '/src/views/');

/* Loader
 */
if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = 'index';
}

switch ($page) {
	case 'persegi'			: new Persegi(); break;
	case 'persegi-panjang'	: new PersegiPanjang(); break;
	case 'lingkaran'		: new Lingkaran(); break;
	case 'belah-ketupat'	: new BelahKetupat(); break;
	case 'segitiga'			: new Segitiga(); break;
	default					: new Index(); break;
}