<?php

namespace src\libraries\Rumus;

class Segitiga extends Rumus
{
	private $alas		= 0;
	private $tinggi		= 0;
	private $sisiA		= 0;
	private $sisiB		= 0;
	private $sisiC		= 0;
	private $luas		= 0;
	private $keliling	= 0;

	public function setAlas(Int $alas)
	{
		$this->alas = $alas;
	}

	public function setTinggi(Int $tinggi)
	{
		$this->tinggi = $tinggi;
	}

	public function setSisiA(Int $sisi_a) {
		$this->sisi_a = $sisi_a;
	}

	public function setSisiB(Int $sisi_b) {
		$this->sisi_b = $sisi_b;
	}

	public function setSisiC(Int $sisi_c) {
		$this->sisi_c = $sisi_c;
	}

	public function hitungLuas()
	{
		$this->luas = 0.5 * ($this->alas * $this->tinggi);
	}

	public function hitungKeliling()
	{
		$this->keliling = $this->sisi_a + $this->sisi_b + $this->sisi_c;
	}

	public function getLuas()
	{
		return $this->luas;
	}

	public function getKeliling()
	{
		return $this->keliling;
	}
}