<?php

namespace src\controllers;

use src\libraries\Rumus;

class Persegi
{
	function __construct()
	{
		$data = ['sisi' => 0, 'bangun_datar' => null];

		/* untuk menghitung luas atau keliling dari persegi
		 * terlebih dahulu ketahui panjang sisi dari persegi
		 */
		if (isset($_POST['sisi'])) {
			$data['sisi'] = (int) $_POST['sisi'];
		}

		$data['bangun_datar'] = new Rumus\Persegi();
		$data['bangun_datar']->setSisi($data['sisi']);

		if (isset($_POST['luas'])) {
			$data['bangun_datar']->hitungLuas();
		} else if (isset($_POST['keliling'])) {
			$data['bangun_datar']->hitungKeliling();
		}

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'persegi.php';
		include VIEW_PATH . 'footer.php';
	}
}