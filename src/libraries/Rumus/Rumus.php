<?php

namespace src\libraries\Rumus;

abstract class Rumus
{
	/**
	 * Hitung luas dari bangun datar
	 *
	 * @return	void
	 */
	abstract public function hitungLuas();

	/**
	 * Hitung keliling dari bangun datar
	 *
	 * @return	void
	 */
	abstract public function hitungKeliling();

	/**
	 * Ambil luas bangun datar yang telah di hitung
	 *
	 * @return	integer
	 */
	abstract public function getLuas();

	/**
	 * Ambil keliling dari bangun datar yang telah di hitung
	 *
	 * @return	integer
	 */
	abstract public function getKeliling();
}