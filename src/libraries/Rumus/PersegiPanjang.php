<?php

namespace src\libraries\Rumus;

class PersegiPanjang extends Rumus
{
	private $panjang	= 0;
	private $lebar		= 0;
	private $luas		= 0;
	private $keliling	= 0;

	public function setPanjang(Int $panjang)
	{
		$this->panjang = $panjang;
	}

	public function setLebar(Int $lebar)
	{
		$this->lebar = $lebar;
	}

	public function hitungLuas()
	{
		$this->luas = $this->panjang * $this->lebar;
	}

	public function hitungKeliling()
	{
		$this->keliling = 2 * ($this->panjang + $this->lebar);
	}

	public function getLuas()
	{
		return $this->luas;
	}

	public function getKeliling()
	{
		return $this->keliling;
	}
}