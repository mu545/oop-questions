<?php

namespace src\controllers;

use src\libraries\Rumus;

class BelahKetupat
{
	function __construct()
	{
		$data = ['diagonal1' => 0, 'diagonal2' => 0, 'sisi' => 0, 'bangun_datar' => null];

		/* untuk menghitung luas atau keliling dari belah ketupat
		 * terlebih dahulu ketahui kedua diameter, sisi dari belah ketupat
		 */
		if (isset($_POST['diagonal1'])) {
			$data['diagonal1'] = (int) $_POST['diagonal1'];
		}

		if (isset($_POST['diagonal2'])) {
			$data['diagonal2'] = (int) $_POST['diagonal2'];
		}

		if (isset($_POST['sisi'])) {
			$data['sisi'] = (int) $_POST['sisi'];
		}

		$data['bangun_datar'] = new Rumus\BelahKetupat();
		$data['bangun_datar']->setDiagonal1($data['diagonal1']);
		$data['bangun_datar']->setDiagonal2($data['diagonal2']);
		$data['bangun_datar']->setSisi($data['sisi']);

		if (isset($_POST['luas'])) {
			$data['bangun_datar']->hitungLuas();
		} else if (isset($_POST['keliling'])) {
			$data['bangun_datar']->hitungKeliling();
		}

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'belah-ketupat.php';
		include VIEW_PATH . 'footer.php';
	}
}