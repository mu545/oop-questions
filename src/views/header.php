<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bangun Datar</title>
    <style>
        * {
            padding: 0;
            margin: 0;
            font-family: sans-serif;
        }
        body {
            display: flex;
            flex-wrap: wrap;
            padding: 1rem;
        }
        form, #result, .box {
            min-height: 200px;
            width: 35%;
            margin: 1rem;
            padding: 1rem;
            border: 1px solid black;
            flex-grow: 1;
            text-align: center;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        form h1 {
            margin-bottom: 1rem;
        }
        input[type=number] {
            margin: .5rem auto;
            padding: .5rem
        }
        button[type=submit] {
            margin: .7rem .3rem;
            padding: .5rem;
            border: none;
            color: #FFF;
        }
        button.btn-luas {
            background: #FC8585;
        }
        button.btn-keliling {
            background: #46C5DD;
        }
        button[type=submit]:hover {
            cursor: pointer;
        }
        #navigation {
            width: 100%;
            padding: 1rem;
            overflow: hidden;
            display: block;
        }
        #navigation a {
            color: #666666;
            width: auto;
            padding: 10px;
            display: inline-block;
            background: #F0F0F0;
            text-decoration: none;
        }
        #result p {
            padding: 10px;
            display: block;
        }
        #result p.luas {
            color: #FC8585;
        }
        #result p.keliling {
            color: #46C5DD;
        }
        .box a{
            color: #FFFFFF;
            padding: 10px;
            display: block;
            margin-top: 50px;
            text-decoration: none;
            background: #999999;
        }
        .box a:hover {
            background: #666666;
        }
    </style>
</head>
<body>
    <nav id="navigation">
        <a href="?page=persegi">Persegi</a>
        <a href="?page=persegi-panjang">Persegi panjang</a>
        <a href="?page=lingkaran">Lingkaran</a>
        <a href="?page=belah-ketupat">Belah ketupat</a>
        <a href="?page=segitiga">segitiga</a>
    </nav>