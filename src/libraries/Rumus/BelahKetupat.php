<?php

namespace src\libraries\Rumus;

class BelahKetupat extends Rumus
{
	private $diagonal1	= 0;
	private $diagonal2	= 0;
	private $sisi		= 0;
	private $luas		= 0;
	private $keliling	= 0;

	public function setDiagonal1(Int $diagonal)
	{
		$this->diagonal1 = $diagonal;
	}

	public function setDiagonal2(Int $diagonal)
	{
		$this->diagonal2 = $diagonal;
	}

	public function setSisi(Int $sisi)
	{
		$this->sisi = $sisi;
	}

	public function hitungLuas()
	{
		$this->luas = 0.5 * ($this->diagonal1 * $this->diagonal2);
	}

	public function hitungKeliling()
	{
		$this->keliling = 4 * $this->sisi;
	}

	public function getLuas()
	{
		return $this->luas;
	}

	public function getKeliling()
	{
		return $this->keliling;
	}
}