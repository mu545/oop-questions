<?php

namespace src\libraries\Rumus;

class Lingkaran extends Rumus
{
	private $diameter	= 0;
	private $jjari		= 0;
	private $pi			= false;
	private $luas		= 0;
	private $keliling	= 0;

	public function setDiameter(Int $diameter)
	{
		$this->diameter = $diameter;
		$this->jjari = $diameter / 2;

		/* tentukan nilai pi yang akan di pakai
		 * n ada 2 jenis yaitu 22 per 7 dan 3.14
		 * jika r adalah 7 atau kelipatan dari 7
		 * maka nilai dari n adalah 22 per 7
		 * dan selain itu nilai dari n adalah 3.14
		 */
		if (($this->jjari % 7) == 0) {
			// gunakan pi 22 per 7
			$this->pi = true;
		} else {
			$this->jjari2 = $this->jjari;
		}
	}

	public function hitungLuas()
	{
		// n * r * r
		if ($this->pi) {
			$this->luas = (22 * $this->jjari) * ($this->jjari / 7);
		} else {
			$this->luas = 3.14 * ($this->jjari * $this->jjari);
		}

	}
	public function hitungKeliling()
	{
		// 2 * n * r
		if ($this->pi) {
			$this->keliling = (2 * 22) * ($this->jjari / 7);
		} else {
			$this->keliling = (2 * 3.14) * $this->jjari;
		}
	}

	public function getLuas()
	{
		return $this->luas;
	}

	public function getKeliling()
	{
		return $this->keliling;
	}
}