<?php

namespace src\controllers;

use src\libraries\Rumus;

class Lingkaran
{
	function __construct()
	{
		$data = ['diameter' => 0, 'bangun_datar' => null];

		/* untuk menghitung luas atau keliling dari lingkaran
		 * terlebih dahulu ketahui diameter dari lingkaran
		 */
		if (isset($_POST['diameter'])) {
			$data['diameter'] = (int) $_POST['diameter'];
		}

		$data['bangun_datar'] = new Rumus\Lingkaran();
		$data['bangun_datar']->setDiameter($data['diameter']);

		if (isset($_POST['luas'])) {
			$data['bangun_datar']->hitungLuas();
		} else if (isset($_POST['keliling'])) {
			$data['bangun_datar']->hitungKeliling();
		}

		include VIEW_PATH . 'header.php';
		include VIEW_PATH . 'lingkaran.php';
		include VIEW_PATH . 'footer.php';
	}
}